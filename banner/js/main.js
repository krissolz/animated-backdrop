(function (d) {

    (function (w) {

        if (typeof w.CustomEvent === "function") return false;

        function CustomEvent(event, params) {
            params = params || { bubbles: false, cancelable: false, detail: null };
            var evt = document.createEvent('CustomEvent');
            evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
            return evt;
        }

        w.CustomEvent = CustomEvent;
    })(window);

    var DRG = {

        obj: null,

        evt: d.createEvent('CustomEvent'),

        init: function (o, oRoot, minX, maxX, minY, maxY, x, y, bSwapHorzRef, bSwapVertRef, fXMapper, fYMapper) {

            o = typeof o === 'object' ? o : $(o)[0];
            oRoot = typeof oRoot === 'object' ? oRoot : $(oRoot)[0];

            o.onmousedown = DRG.start;

            o.hmode = bSwapHorzRef ? false : true;
            o.vmode = bSwapVertRef ? false : true;

            o.root = oRoot && oRoot != null ? oRoot : o;

            o.root.style.left = (o.hmode && !isNaN(x)) ? x + 'px' : o.root.offsetLeft + 'px';
            o.root.style.top = (o.vmode && !isNaN(y)) ? y + 'px' : o.root.offsetTop + 'px';
            o.root.style.right = (!o.hmode && !isNaN(x)) ? x + 'px' : 0;
            o.root.style.bottom = (!o.hmode && !isNaN(x)) ? y + 'px' : 0;

            o.minX = typeof minX != 'undefined' ? minX : null;
            o.minY = typeof minY != 'undefined' ? minY : null;
            o.maxX = typeof maxX != 'undefined' ? maxX : null;
            o.maxY = typeof maxY != 'undefined' ? maxY : null;

            o.xMapper = fXMapper ? fXMapper : null;
            o.yMapper = fYMapper ? fYMapper : null;

            o.root.onDragStart = new Function;
            o.root.onDragEnd = new Function;
            o.root.onDrag = new Function;
        },

        start: function (e) {
            var o = DRG.obj = this;
            e = DRG.fixE(e);
            var y = parseInt(o.vmode ? o.root.style.top : o.root.style.bottom);
            var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right);

            o.root.onDragStart(x, y);

            o.lastMouseX = e.clientX;
            o.lastMouseY = e.clientY;

            if (o.hmode) {
                if (o.minX != null) o.minMouseX = e.clientX - x + o.minX;
                if (o.maxX != null) o.maxMouseX = o.minMouseX + o.maxX - o.minX;
            } else {
                if (o.minX != null) o.maxMouseX = -o.minX + e.clientX + x;
                if (o.maxX != null) o.minMouseX = -o.maxX + e.clientX + x;
            }

            if (o.vmode) {
                if (o.minY != null) o.minMouseY = e.clientY - y + o.minY;
                if (o.maxY != null) o.maxMouseY = o.minMouseY + o.maxY - o.minY;
            } else {
                if (o.minY != null) o.maxMouseY = -o.minY + e.clientY + y;
                if (o.maxY != null) o.minMouseY = -o.maxY + e.clientY + y;
            }

            d.onmousemove = DRG.drag;
            d.onmouseup = DRG.end;

            return false;
        },

        drag: function (e) {
            e = DRG.fixE(e);
            var o = DRG.obj;

            var ey = e.clientY;
            var ex = e.clientX;
            var y = parseInt(o.vmode ? o.root.style.top : o.root.style.bottom);
            var x = parseInt(o.hmode ? o.root.style.left : o.root.style.right);
            var nx, ny;

            if (o.minX != null) ex = o.hmode ? Math.max(ex, o.minMouseX) : Math.min(ex, o.maxMouseX);
            if (o.maxX != null) ex = o.hmode ? Math.min(ex, o.maxMouseX) : Math.max(ex, o.minMouseX);
            if (o.minY != null) ey = o.vmode ? Math.max(ey, o.minMouseY) : Math.min(ey, o.maxMouseY);
            if (o.maxY != null) ey = o.vmode ? Math.min(ey, o.maxMouseY) : Math.max(ey, o.minMouseY);

            nx = x + ((ex - o.lastMouseX) * (o.hmode ? 1 : -1));
            ny = y + ((ey - o.lastMouseY) * (o.vmode ? 1 : -1));

            if (o.xMapper) nx = o.xMapper(y)
            else if (o.yMapper) ny = o.yMapper(x)

            o.root.style[o.hmode ? "left" : "right"] = nx + "px";
            o.root.style[o.vmode ? "top" : "bottom"] = ny + "px";
            o.lastMouseX = ex;
            o.lastMouseY = ey;

            /* this writes togeter with drag action */
            o.root.onDrag(nx, ny);

            DRG.evt = new CustomEvent('slide', {
                bubbles: true,
                cancelable: true,
                detail: {
                    value: Math.floor(nx / o.maxX * 100),
                    valueY: Math.floor(ny / o.maxY * 100)
                }
            });
            o.dispatchEvent(DRG.evt);

            return false;
        },

        end: function () {
            d.onmousemove = null;
            d.onmouseup = null;

            /* this writes end result of the drag action */
            DRG.obj.root.onDragEnd(parseInt(DRG.obj.root.style[DRG.obj.hmode ? "left" : "right"]),
                parseInt(DRG.obj.root.style[DRG.obj.vmode ? "top" : "bottom"]));

            DRG.obj = null;
        },

        fixE: function (e) {
            if (typeof e == 'undefined') e = w.event;
            if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
            if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
            return e;
        }
    };

    $.fn.slider = function () {
        var o = this;

        if (!o.length) return;

        o.addClass('tracker');

        for (var t = 0; t <= o.length - 1; t++)
            if (!$(o[t]).find('.slider').length)
                $(o[t]).append('<div class="slider"><div class="holder"></div></div>');

        var s = o.find('.slider'),
            h = o.find('.holder'),
            track = o.width() - s.outerWidth();

        s.on('mousedown', function () { $(this).addClass('active'); });
        s.on('mouseup', function () { $(this).removeClass('active'); });

        for (var x = 0; x <= o.length - 1; x++) DRG.init(h[x], s[x], 0, track, 0, 0, track / 2, 0);
    }

    $.params = ((d.location.href.split('#')[1] || '').split('&')).reduce(function (params, param) {
        var param = param.split('=');
        params[param[0]] = decodeURIComponent(param.slice(1).join('='));
        return params;
    }, {});

    $(function () {
        var s = $('.wrapper');
        s.slider();
        s.on('slide', sliderHandler);

        $('#creativeLink').attr('href', $.params.clickTag);
    });

    function sliderHandler(e) {
        var v = e.detail.value;
        if (v >= 34 && v < 66) goMiddle();
        else if (v >= 67 && v <= 100) goRight();
        else if (v >= 0 && v < 33) goLeft();
    }

    function goLeft() {
        $('.leftLayout').addClass('expandBg');
        $('.leftLayout').removeClass('collapseBg');
        $('.rightLayout').addClass('collapseBg');
        $('.rightLayout').removeClass('expandBg');
    }

    function goRight() {
        $('.leftLayout').removeClass('expandBg');
        $('.leftLayout').addClass('collapseBg');
        $('.rightLayout').removeClass('collapseBg');
        $('.rightLayout').addClass('expandBg');
    }

    function goMiddle() {
        $('.leftLayout, .rightLayout').removeClass('expandBg collapseBg');
    }

})(document);