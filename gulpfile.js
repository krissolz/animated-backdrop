var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var merge = require('merge-stream');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var terser = require('gulp-terser');
var htmlmin = require('gulp-htmlmin');
var imagemin = require('gulp-imagemin');

var desktopAssets = {
    scss: 'banner/scss/**/*.scss',
    js: 'banner/js/**/*.js',
    html: 'banner/*.html',
    img: 'banner/i/*'
};

var desktopDest = {
    js: 'dist/js/',
    css: 'dist/css/',
    html: 'dist',
    img: 'dist/i/'
};

gulp.task('images', function () {
    return gulp.src(desktopAssets.img)
    .pipe(imagemin())
    // .pipe(imagemin([
    //     imagemin.gifsicle({interlaced: true}),
    //     imagemin.jpegtran({progressive: true}),
    //     imagemin.optipng({optimizationLevel: 5}),
    //     imagemin.svgo({
    //         plugins: [
    //             {removeViewBox: true},
    //             {cleanupIDs: false}
    //         ]
    //     })
    // ]))
    .pipe(gulp.dest(desktopDest.img));
});

gulp.task('scripts', function () {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        desktopAssets.js
    ])
        .pipe(concat('main.js'))
        //.pipe(gulp.dest(desktopDest.js))
        .pipe(terser())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest(desktopDest.js));
});

gulp.task('styles', function () {
    var sassStream,
        cssStream;

    sassStream = gulp.src(desktopAssets.scss)
        .pipe(sourcemaps.init())
        .pipe(sass()).on('error', sass.logError)
        .pipe(autoprefixer({ browserlist: ['last 2 versions'], cascade: false }));
    //.pipe(gulp.dest(desktopDest.css));

    cssStream = gulp.src([
        'node_modules/normalize.css/normalize.css'
    ]);

    return merge(sassStream, cssStream)
        .pipe(concat('main.min.css'))
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        //.pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(desktopDest.css));
});

gulp.task('html', function () {
    return gulp.src(desktopAssets.html)
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(desktopDest.html));
});

gulp.task('watch', function () {
    gulp.watch(desktopAssets.scss, gulp.series('styles'));
    gulp.watch(desktopAssets.js, gulp.series('scripts'));
    gulp.watch(desktopAssets.html, gulp.series('html'));
    gulp.watch(desktopAssets.img, gulp.series('images'));
});

gulp.task('build', gulp.series('scripts', 'styles', 'html', 'images'));
